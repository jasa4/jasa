import 'package:flutter/material.dart';
import 'dart:html' as html; // Untuk fungsi refresh halaman di web
import 'dart:async'; // Untuk timer

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tinggal Ngopi !!!',
      theme: ThemeData(
        primarySwatch: const Color.blue,
      ),
      home: const AutoRefreshPage(),
    );
  }
}

class AutoRefreshPage extends StatefulWidget {
  const AutoRefreshPage({super.key});

  @override
  _AutoRefreshPageState createState() => _AutoRefreshPageState();
}

class _AutoRefreshPageState extends State<AutoRefreshPage> {
  int _counter = 30; // Waktu countdown dalam detik
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    // Timer untuk countdown
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_counter > 0) {
        setState(() {
          _counter--;
        });
      } else {
        _timer.cancel();
        _refreshPage(); // Panggil refresh ketika countdown selesai
      }
    });
  }

  // Fungsi untuk refresh halaman
  void _refreshPage() {
    setState(() {
      // Pesan sebelum halaman di-refresh
    });
    html.window.location.reload(); // Refresh halaman
  }

  @override
  void dispose() {
    _timer.cancel(); // Membatalkan timer ketika widget dihancurkan
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Wolu,Mantap !!!!!'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Wolu !!!:',
              style: TextStyle(fontSize: 20),
            ),
            const SizedBox(height: 20),
            Text(
              '$_counter detik',
              style: const TextStyle(
                  fontSize: 50, fontWeight: FontWeight.bold, color: Colors.red),
            ),
            const SizedBox(height: 20),
            const Text(
              'Ngopi Dulu...',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
}